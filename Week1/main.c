/*
 * Week1.c
 *
 * Created: 30/01/2019 10:18:59
 * Author : Tim Herreijgers
 */ 

#define F_CPU 8000000L

#include <avr/io.h>
#include <util/delay.h>
#include <inttypes.h>
#define BIT(x) (1 << (x))

void opdrachtB3()
{
	if(PINC & BIT(7))
	{
		PORTD ^= BIT(PORTD6);
		_delay_ms(100);
	}
	else
	{
		PORTD = ~BIT(PORTD7);
	}
}

void opdrachtB4()
{
	uint8_t i;
	for(i = 0; i < 8; i++)
	{
		PORTD = BIT(i);
		_delay_ms(50);
	}
	PORTD = 0x00;
}

void B5();

void opdrachtB6()
{
	if(PINC & BIT(PORTC0))
	{
		PORTA ^= BIT(PORTA0);
	}
	
	PORTD ^= BIT(PORTD7);
	
	if(PORTA == BIT(PORTA0))
	{
		_delay_ms(4000);
	}
	else
	{
		_delay_ms(1000);
	}
}

void opdrachtB7a()
{
	if(PINC & BIT(PORTC0))
	{
		PORTB = 0x00;
		PORTB = BIT(PORTB0);
	}
	else if(PINC & BIT(PORTC1))
	{
		PORTB = 0x00;
		PORTB = BIT(PORTB1);
	}
	else if(PINC & BIT(PORTC2))
	{
		PORTB = 0x00;
		PORTB = BIT(PORTB2);
	}
	
	switch(PORTB)
	{
		case BIT(PORTB0):
		opdrachtB3();
		break;
		case BIT(PORTB1):
		opdrachtB4();
		break;
		case BIT(PORTB2):
		opdrachtB6();
		break;
	}
}

int main(void)
{
	B5();
}

void B5() 
{
	unsigned char values[] = {
		0b00011000,
		0b00100100,
		0b01000010,
		0b10000001,
		0b10000001,
		0b01000010,
		0b00100100,
		0b00011000,
		0b11111111,
		0b00000000,
		0b11111111,
		0b01111110,
		0b00111100,
		0b00011000,
		0b00000000
	};
	
	DDRA = 0b11111111;
		
	while(1)
	{
		for(int index = 0; index < 15; index++)
		{
			PORTA = values[index];
			_delay_ms(500);
		}
	}
}
