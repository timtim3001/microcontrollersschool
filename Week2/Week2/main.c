/*
 * Week2.c
 *
 * Created: 2/6/2019 10:49:07
 * Author : Rick,Tim,Thomas
 */ 
#define F_CPU 8e6

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void B2();
void B3();
void B4();

int main(void)
{
    B4();
}

	
#pragma region Opdracht B2
int8_t value;
ISR(INT1_vect){
	if(value < 8){
		value++;
	}
	PORTA = (1<<value);
}

ISR(INT2_vect){
	if(value > 0){
		value--;
	}
	PORTA = (1<<value);
}

void B2(){
	value = 0;
	DDRA = 0xFF;
	PORTA = (1<<value);
	EICRA |= (1 << ISC11) | (1<< ISC10) | (1 << ISC21) | (1<< ISC20);
	EIMSK |= (1 << INT1) | (1 << INT2);
	
	sei();
	while (1)
	{
		
	}
}
#pragma endregion Opdracht B2

#pragma region Opdracht B3
int numbers[16] = {
		0b00111111, //0
		0b00000110,	//1
		0b01011011,	//2
		0b01001111,	//3
		0b01100110,	//4
		0b01101101,	//5
		0b01111101,	//6
		0b00000111,	//7
		0b01111111,	//8
		0b01101111,	//9
		0b01110111,	//A
		0b01111100,	//B
		0b00111001, //C
		0b01011110, //D
		0b01111001, //E
		0b01110001  //F	
	};
	
void setDisplay(int numbers[], int val){
	PORTD = numbers[val];
}

void B3(){
	int value = 0;
	DDRD = 0xFF;
	while(1){
		if(PINA == (1 << PINA0)){
			value++;
		}
		if(PINA == (1 << PINA1)){
			value--;
		}
		if(value < 0 || value > 15){
			setDisplay(numbers, 14);
		} else {
			setDisplay(numbers, value);
		}
		_delay_ms(100);
	}	
}

#pragma endregion Opdracht B3

#pragma region Opdracht B4
int animatie[8] = {
	0b00000001,
	0b00000010,
	0b00000100,
	0b00001000,
	0b00010000,
	0b00100000,
	0b01000000,
	0b01111111
};

void B4(){
	int value = 0;
	DDRD = 0xFF;
	while(1){
		value ++;
		if(value == 8)
			value = 0;
		setDisplay(animatie, value);
		_delay_ms(250);
	}
}
#pragma endregion Opdracht B4

