/*
 * PwmSpeaker.c
 *
 * Created: 3/13/2019 10:46:59
 * Author : Rick
 */ 

#define F_CPU 8e6

#include <avr/io.h>
#include <avr/interrupt.h>
#include <math.h>
#include <util/delay.h>
#include "marioTheme.h"

void initPwm(void);
void initAdc(void);
void playMario(void);
#define getTopFromFreq(x) ((F_CPU / (x * 2)) - 1)

int freqs[6] = {400, 500, 600, 700, 800, 900};
char sensorTouched[6] = {0, 0, 0, 0, 0, 0};
char currentConverter = 0;

int main(void)
{
	char count = 0;
	int totalFreq = 0;
	
  initPwm();
	initAdc();
	sei();
	
	OCR1A = 0;
	
  while (1) 
  {
		for(int i = 0; i < 6; i++)
		{
			if(sensorTouched[i] == 1) {
				totalFreq += freqs[i];
				count++;
			}
		}
		
		if(count == 0)
			OCR1A = 0;
		else
			OCR1A = getTopFromFreq(totalFreq / count);
			
		count = 0;
		totalFreq = 0;
		_delay_ms(25);
  }
}

void initPwm() {
	DDRB |= (1 << PB5);
	TCCR1B |= (1 << WGM12);
	TCCR1A |= (1 << COM1A0);
	
	OCR1A = 256;
	TCCR1B |= (1 << CS10);
}

void initAdc(void)
{
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1);
	ADMUX |= (1 << REFS0);
	ADCSRA |= (1 << ADEN) | (1 << ADIE) | (1 << ADSC);
}

void playMario(void)
{
	DDRD = 0xFF;
	int i;
	for(i = 0; i < 203; i++)
	{
		if(melody[i] == 0)
		{
			OCR1A = 0;
			_delay_ms(1000 / tempo[i]);
			continue;
		}
		
		OCR1A = getTopFromFreq(melody[i]);
		_delay_ms(1000 / tempo[i]);
	}
}

ISR(ADC_vect)
{
	if(ADC < 500)
		sensorTouched[currentConverter] = 1;
	else
		sensorTouched[currentConverter] = 0;
		
	currentConverter = currentConverter + 1 < 6 ? currentConverter + 1 : 0;
	ADMUX &= 0b11100000;
	ADMUX |= (currentConverter);
	ADCSRA |= (1 << ADSC);
}
