/*
 * Week3.c
 *
 * Created: 13-2-2019 10:58:38
 * Author : thoma
 */ 

#define F_CPU 8e6

#include "lcd.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void B1();
void B2();
void B3();

int main(void)
{
	B3();
}

void B1()
{
	lcd_init();
	char *text = "Tim, Thomas";
	char *text2 = "en Rick";
	
	lcd_setCursor(5);
	lcd_displayTextWrap(text, 11);
	lcd_setCursor(21);
	lcd_displayText(text2, 7);

	while (1)
	{
	}
}

int value = 0;

int getAmountOfTens(int value)
{
	int counter = 0;
	while(value >= 10)
	{
		counter++;
		value = value / 10;
	}
	return counter;
}

void B2()
{
	lcd_init();
	EICRA |= (1 << ISC11) | (1<< ISC10);
	EIMSK |= (1 << INT1);
	
	sei();

	char myString[7];

	while (1)
	{
		itoa(value, myString, 10);
		lcd_setCursor(0);
		lcd_displayText(myString, getAmountOfTens(value) + 1);
		_delay_ms(100);
	}
}

ISR(INT1_vect){
	value++;
}

void B3()
{
	
	DDRD = 0xFF;
	OCR2 = 116;
	
	TCCR2 = (1<<CS22) | (1<<CS20);
	sei();
	
	while(1)
	{
		
	}
}

ISR(TIMER2_COMP_vect)
{
	PORTD ^= (1<<PD7);
	
	if(OCR2 == 116)
		OCR2 = 194;
	else
		OCR2 = 116;
}







