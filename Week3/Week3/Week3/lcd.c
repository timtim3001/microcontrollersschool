/*
 * lcd.c
 *
 * Created: 13-2-2019 10:59:21
 *  Author: thoma
 */ 

#define F_CPU 8e6

#include "lcd.h"
#include <util/delay.h>
#include <avr/io.h>

void lcd_displayChar(unsigned char text);
void lcd_command(const unsigned char command);

void lcd_init()
{
	//Set the output register
	DDRC = 0xFF;
	lcd_command(0x02);
	lcd_command(0x28);
	lcd_command(0x0E);
	lcd_command(0x01);
	lcd_command(0x80);
		
	//PORTC |= 0x08 | (0x41 >> 4);
	//_delay_ms(1);
	//PORTC = 0x04;
	//
	//PORTC |= 0x08 | (0x41 >> 4);
	//_delay_ms(1);
	//PORTC = 0x04;
}

void lcd_displayTextWrap(const char *text, const uint8_t size)
{
	if(size > 16)
	{
		for(int i = 0; i < 16; i++)
			lcd_displayChar(text[i]);
			
		lcd_command(0xC0);
		
		for(int i = 16; i < size; i++)
			lcd_displayChar(text[i]);
	} 
	else 
	{
		lcd_displayText(text, size);
	}
}

void lcd_displayText(const char *text, const uint8_t size)
{	
	for(int i = 0; i < size; i++)
		lcd_displayChar(text[i]);
}

void lcd_setCursor(int position)
{
	uint8_t row = position / 16;
	uint8_t column = position % 16;
	
	if(row == 0)
		lcd_command(0x80);
	else
		lcd_command(0xC0);
		
	for(int i = 0; i < column; i++) {
		lcd_command(0x14);
	}
}

void lcd_displayChar(unsigned char text)
{
	PORTC = (text & 0xF0) | 0x0C;
	_delay_ms(1);
	PORTC = 0x04;
	_delay_ms(1);
	
	PORTC = ((text & 0x0F) << 4) | 0x0C;
	_delay_ms(1);
	PORTC = 0x00;
	_delay_ms(1);
}

void lcd_command(const unsigned char command)
{
	PORTC = (command & 0xF0) | 0x08;
	_delay_ms(1);
	PORTC = 0x04;
	_delay_ms(1);
	
	PORTC = ((command & 0x0F) << 4) | 0x08;
	_delay_ms(1);
	PORTC = 0x00;
	_delay_ms(1);
}














