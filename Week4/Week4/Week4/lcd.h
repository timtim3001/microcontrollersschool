/*
 * lcd.h
 *
 * Created: 13-2-2019 10:59:36
 *  Author: thoma
 */ 


#ifndef LCD_H_
#define LCD_H_

#include <inttypes.h>

void lcd_init();
void lcd_displayText(const char *text, uint8_t size);
void lcd_displayTextWrap(const char *text, uint8_t size);
void lcd_setCursor(int position);




#endif /* LCD_H_ */