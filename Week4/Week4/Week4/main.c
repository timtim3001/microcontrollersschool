/*
 * Week4.c
 *
 * Created: 20-2-2019 10:25:21
 * Author : thoma
 */ 

#define F_CPU 8e6

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include <stdlib.h>
#include "lcd.h"

void setupAdc(bool freeRunning)
{
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1);
	ADMUX |= (1 << REFS0);
	ADCSRA |= (1 << ADEN);
	
	if(freeRunning)
		ADCSRA |= (1 << ADFR) | (1 << ADSC);
}

void B1()
{
	setupAdc(true);
	//Enable adc interrupt
	ADCSRA |= (1 << ADIE);
	sei();
	
	while (1)
	{
	}
}

ISR(ADC_vect)
{
	PORTA = ADCL;
	PORTB = ADCH;
}

void B2()
{
	setupAdc(false);
	
	//Enable adc interrupt
	ADCSRA |= (1 << ADIE);
	sei();
	
	while (1)
	{
		ADCSRA |= (1 << ADSC);
		
		_delay_ms(1000);
	}
}

int getAmountOfTens(int value)
{
	int counter = 0;
	while(value >= 10)
	{
		counter++;
		value = value / 10;
	}
	return counter;
}

void B3()
{
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1);
	ADMUX |= (1 << REFS0) | (1 << REFS1);
	ADCSRA |= (1 << ADEN);
		
	lcd_init();
	
	
	char myString[7] = {'\0','\0','\0','\0','\0','\0','\0'};
	while(1)
	{
		ADCSRA |= (1 << ADSC);
		while(!(ADCSRA & (1 << ADIF)));
		int value = (ADC & 0xFF);
		value = value / 4;
		value += 2;
		
		itoa(value, myString, 10);
		lcd_setCursor(0);
		lcd_displayText("Temp: ", 6);
		lcd_displayText(myString, getAmountOfTens(value) + 1);
		
		PORTA = ADCL;
		PORTB = ADCH;
		
		_delay_ms(1000);
	}
}

int main(void)
{
	DDRB = 0xFF;
	DDRA = 0xFF;
	B3();    
}

